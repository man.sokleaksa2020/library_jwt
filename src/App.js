import React from "react";
import Home from "./views/Home";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Menu from "./components/Menu";
import { Container } from "react-bootstrap";
import LoginWithFacebook from "./views/LoginWithFacebook";
import LoginWithGoogle from "./views/LoginWithGoogle";
import "./App.css"
import LoginWithJWT from "./views/LoginWithJWT";

export default function App() {
  return (
    <BrowserRouter>
      <Menu />
      <Container>
        <Switch>
          <Route path="/" exact component={LoginWithJWT} />
        </Switch>
      </Container>
    </BrowserRouter>
  );
}
