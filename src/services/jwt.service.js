import axios from "axios"
import CryptoJS from "crypto-js"

const JWT_API = axios.create({
    baseURL: "http://110.74.194.124:9999/api"
})

export const signup = async(user) => {
    try {
        const result = await JWT_API.post("/auth/register", user)
        console.log("signup:", result);
        return result
    } catch (error) {
        console.log("signup error:", error);
    }
}

export const signin = async(user) => {
    try {
        const result = await JWT_API.post("/auth/login", user)
        console.log("signin:", result.data.payload.token);
        return result.data.payload.token
    } catch (error) {
        console.log("signin error:", error);
    }
}

export const add_post = async(post) => {
    try {
        const token = localStorage.getItem("token");
        let decr = CryptoJS.AES.decrypt(token, "123").toString(CryptoJS.enc.Utf8)

        const result = await JWT_API.post("/posts/create", post ,{
            headers: {
                Authorization: "Bearer "+decr
            }
        })
        console.log("add_post:", result);
        return result
    } catch (error) {
        console.log("add_post error:", error);
    }
}