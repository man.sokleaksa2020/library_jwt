import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import tutorialReducer from '../reducers/tutorialReducer'

const rootReducers = combineReducers({
    tutorialReducer,
})

export const store = createStore(rootReducers, applyMiddleware(thunk))