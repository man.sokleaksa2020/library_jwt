import { ADD_TUTORIAL, DELETE_TUTORIAL_BY_ID, FETCH_ALL_TUTORIALS } from "../actions/tutorialAction"

const initState = {
    tutorials: [],
}

export default function tutorialReducer(state = initState, {type, payload}){
    //console.log("tutorialReducer action:", action);
    switch(type){
        case FETCH_ALL_TUTORIALS:
            return{
                ...state,
                tutorials: payload
            }
        case DELETE_TUTORIAL_BY_ID:
            return{
                ...state,
                tutorials: state.tutorials.filter(item=>item._id != payload)
            }
        case ADD_TUTORIAL:
            return{
                ...state,
                tutorials: [...state.tutorials, payload]
            }
        default:
            return state
    }

}