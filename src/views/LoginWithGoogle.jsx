import React from "react";
import { GoogleLogin } from 'react-google-login';

export default function LoginWithGoogle() {
  //711714507136-si3tvs0eesk1iid9nrc4gj849hpv7h30.apps.googleusercontent.com

  const responseGoogle = (response) => {
    console.log(response);
  }

  return (
    <div>
      <GoogleLogin
        clientId="711714507136-si3tvs0eesk1iid9nrc4gj849hpv7h30.apps.googleusercontent.com"
        buttonText="Login"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={"single_host_origin"}
      />
    </div>
  );
}
