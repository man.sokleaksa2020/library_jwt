import React, { useEffect, useState } from "react";
import { Table, Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
    addTutorial,
  deleteTutorialById,
  fetchAllTutorials,
} from "../redux/actions/tutorialAction";
import { add_tutorial } from "../services/tutorials.service";

export default function Home() {
  const dispatch = useDispatch();
  const { tutorials } = useSelector((state) => state.tutorialReducer);
  //console.log("DATA:", data);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [imageURL, setImageURL] = useState("");

  useEffect(() => {
    dispatch(fetchAllTutorials());
    
  }, []);

  function onDeleteTutorialById(id) {
    dispatch(deleteTutorialById(id));
  }

  async function onAddTutorial() {
      const tutorial = {
          title,
          description,
          thumbnail: imageURL
      }
    dispatch(addTutorial(tutorial))
    //const result = await add_tutorial(tutorial)
    //dispatch(fetchAllTutorials());

  }

  return (
    <>
      <div className="d-flex justify-content-between my-4">
        <h1>Tutorials</h1>
        {/* <Button variant="success">Add Tutorial</Button> */}
      </div>
      <div>
        <Form>
          <Form.Group controlId="formBasicEmail">
            {/* <Form.Label>Email address</Form.Label> */}
            <Form.Control
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              placeholder="Title"
            />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>
          <br />
          <Form.Group controlId="formBasicEmail">
            {/* <Form.Label>Email address</Form.Label> */}
            <Form.Control
              type="text"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              placeholder="Decription"
            />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>
          <br />
          <Form.Group controlId="formBasicEmail">
            {/* <Form.Label>Email address</Form.Label> */}
            <Form.Control
              type="text"
              value={imageURL}
              onChange={(e) => setImageURL(e.target.value)}
              placeholder="Thumbnail url"
            />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>

          <br />
          <Button onClick={onAddTutorial} variant="primary" type="button">
            Add
          </Button>
        </Form>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Description</th>
            <th>Thumbnail</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {tutorials.map((item, index) => (
            <tr key={index}>
              <td>{item._id.slice(0, 8)}</td>
              <td>{item.title}</td>
              <td>{item.description}</td>
              <td>
                <img width="200px" src={item.thumbnail} />
              </td>
              <td>
                <Button variant="primary">View</Button>{" "}
                <Button variant="warning">Edit</Button>{" "}
                <Button
                  onClick={() => onDeleteTutorialById(item._id)}
                  variant="danger"
                >
                  Delete
                </Button>{" "}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}
