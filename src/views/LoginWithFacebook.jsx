import React, { useState } from "react";
import FacebookLogin from "react-facebook-login";

export default function LoginWithFacebook() {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [picture, setPicture] = useState("");
  const [isLogin, setIsLogin] = useState(false);

  const responseFacebook = (response) => {
    console.log(response);
    setIsLogin(true);
    setName(response.name);
    setEmail(response.email);
    setPicture(response.picture.data.url);
  };
  return (
    <div>
      {isLogin ? (
        <div>
          <h1>{name}</h1>
          <h1>{email}</h1>
          <img src={picture} />
        </div>
      ) : (
        <FacebookLogin
          appId="874270226459037"
          autoLoad={true}
          fields="name,email,picture"
          callback={responseFacebook}
          cssClass="facebook-style"
          icon={
            <img
              width="60px"
              src="https://facebookbrand.com/wp-content/uploads/2019/04/f_logo_RGB-Hex-Blue_512.png?w=512&h=512"
            />
          }
        />
      )}
    </div>
  );
}
