import React, { useEffect } from "react";
import { Button } from "react-bootstrap";
import { add_post, signin, signup } from "../services/jwt.service";
import CryptoJS from "crypto-js";

export default function LoginWithJWT() {
    useEffect(()=>{
        const token = localStorage.getItem("token")
    console.log("TOKENN:", token);
    })

  function onSignup() {
    const user = {
      fullname: "bro KH",
      username: "bro123",
      password: "123",
    };
    signup(user);
  }

  async function onSignin() {
    const user = {
      username: "bro123",
      password: "123",
    };
    const token = await signin(user);
    console.log("TOKEN:", token);

    //let password = process.env
    //console.log("PASSWORD:", password);
    let encr = CryptoJS.AES.encrypt(token, "123").toString();
    localStorage.setItem("token", encr);
  }

  function onAddPost() {
    const post = {
      caption: "Hello JWT",
      image:
        "https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg",
    };

    add_post(post);
  }

  return (
    <div>
      <h1>JWT</h1>
      <Button variant="primary" onClick={onSignup}>Sign up</Button>{' '}
      <Button variant="success" onClick={onSignin}>Sign in</Button>{' '}
      <Button variant="warning" onClick={onAddPost}>Add Post</Button>
    </div>
  );
}
